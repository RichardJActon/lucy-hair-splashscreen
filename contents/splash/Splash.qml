import QtQuick 2.9
import QtQuick.Controls 2.15


Image {
    id: root
    source: "images/black_1080p.png"
    AnimatedImage {
        id: lucy_hair
        source: "images/Lucy-hair.gif"
        anchors.centerIn: parent
        opacity: 0
    }
    SequentialAnimation {
        id: introAnimation1
        running: true
        ParallelAnimation {
            PropertyAnimation {
                property: "opacity"
                target: lucy_hair
                from: 0
                to: 1
                duration: 750
                easing.type: Easing.InOutBack
                easing.overshoot: 1.0
            }
        }
    }
}
