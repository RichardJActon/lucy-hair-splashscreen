# Lucy's Hair KDE plasma Splashscreen

A splashscreen for KDE plasma desktop using the animation of Lucyna (Lucy) Kushinada's hair from Cyberpunk edgerunners.

Thanks to AmbarFox09 for [their repo](https://github.com/AmbarFox09/Awoo-Splash/) which showed me how to make an animated KDE plasma splash screen.

## Preview

![Lucy's Hair Preview](./contents/previews/lucy_just_hair_2.png)

## Installation

Clone into: `.local/share/plasma/look-and-feel` and select in KDE settings.

